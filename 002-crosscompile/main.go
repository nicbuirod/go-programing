package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Printf("sistema operativo, %v arquitectura : %v", runtime.GOOS, runtime.GOARCH)
}
